��    �        �         �
     �
     �
  �   �
     x     �     �     �  	   �     �     �     �  
   �     �     �               (     5     B     T     d     p     }     �     �     �     �  L   �     �     
               4     J     O     W  	   e  	   o     y          �     �     �     �  
   �     �  
   �     �     �     �  
     	             /  !   ?     a     m     }     �  
   �  	   �  
   �  �   �     �     �     �     �     �  
   �     �     �     �     �     �     �               4     C     [     b     v     ~     �     �     �     �     �     �     �  	   �                      $   $     I  	   R     \     i     {     �     �     �     �  
   �     �  w   �     5  &   L     s  
   x     �     �     �  	   �  
   �  	   �     �     �     �  $   �     �     �     
          .  )  A     k     �  �   �     W     i  
   �     �     �      �     �     �     �               )     ;     Q     e     {     �     �     �     �     �  
             "  �   +     �     �      �  "   �  $     
   8     C     W     k     �     �  ,   �     �     �     �               /     A     O     X     a     {     �      �     �  1   �     
          2  
   F     Q     i     y  +  �     �  !   �     �               -     A     J     `     o     |  &   �     �     �     �  #   �     #  #   ,     P     a     w  &   �     �     �  (   �       6   .     e     ~  
   �     �  
   �  E   �     �                  ,       M      n      u   
   �      �      �      �   �   �      k!  >   {!     �!     �!     �!     �!     �!      "     "     $"     6"     ?"     Q"  %   X"     ~"     �"     �"     �"     �"        T      \   K   j       N   X   v   (       -       ?       w   Y   Q   8           *   3                 U   k              J   )   M   +   f          Z                 i   h       E   :   r       }   B   e               `           
           [   ^   	   W       P          $                     &           /      z              ;   O   @   1      R   %   s       l           y      7   A       I   {   ,   0   ~      V   q   u   _   a   c   �          m   x       o   H             !   .      '   L   2       >   G   C      n                    #   =   D             4       6   b   "   ]   p   t   5   |       d                 <   9   S   F      g    %s Form %s Forms + Add Field <a href="%s" target="_blank">Pojo Theme</a> is not active. Please activate any theme by Pojo.me before you are using "Pojo Forms" plugin. Add New Add New Form Advanced Akismet Options All Forms Allow Multiple Selections Author Author Email Author Url Background Color Background Opacity Border Color Border Radius Border Width Button Align Button Area Width Button Settings Button Size Button Style Button Text Checkbox Checked Choose Form: Content Copy and paste this shortcode into your Text editor or use with Form Widget. Create a Form Credit Custom Style Custom field deleted. Custom field updated. Date Default Default Value Drop-down Edit Form Email Email Reply-To (Optional) Email Subject Email To Field Label Field Settings Field Size Field Style Field Type Fields Form Form Options Form Style Form data Form draft updated. Form published. Form restored to revision from %s Form saved. Form submitted. Form updated. Forms From Email From Name Hide Label If you are using the Akismet plugin you can also use it as the form's spam filter by setting the fields. Insert the relevant field's ID shortcode according to the following settings, e.g. Author Email: [form-field-1] Input Color Insert Shortcode ID Invalid form. Label Align Label Color Label Size Large List Inline Max Medium Min Name Reply-To (Optional) New Form New massage from "%s" No forms found No forms found in Trash Number One option per line Options Page URL Placeholder Powered by http://pojo.me/ Preview Preview Form Problem with Form setting. Radio Redirect To (Optional) Remote IP Required Rows Search Form Send Set the first option as unselectable Settings Shortcode Shortcode ID Side - Align Left Side - Align Right Small Spam Filter Tel Text Text Color Textarea The Pojo Forms is not supported by this version of %s. Please <a href="%s">upgrade the theme to its latest version</a>. This field is required This form has an error, please fix it. Time Title Form Title: Top URL Unchecked User Agent View Form Width XL XXL Your details were sent successfully! button-alignBlock button-alignCenter button-alignLeft button-alignNone button-alignRight Project-Id-Version: Forms
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-20 19:59+0300
PO-Revision-Date: 2015-07-20 20:01+0300
Last-Translator: Ariel Klikstein <a@arielk.net>
Language-Team: Pojo Them <aryodigital@gmail.com>
Language: he_IL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.8.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_x;_n:1,2;_c,_nc:4c,1,2
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
 טופס %s %s טפסים + הוספת שדה <a href="%s" target="_blank">ערכת עיצוב של Pojo.me</a> אינה פעילה. יש להפעיל ערכת עיצוב של Pojo.me לפני שמשתמשים בתוסף  "Pojo Forms". הוספת חדש הוספת טופס חדש מתקדם אפשרויות Akismet כל הטפסים לאפשר בחירה מרובה מחבר אימייל מחבר URL מחבר צבע רקע שקיפות רקע צבע בורדר עיגול מסגרת רוחב מסגרת יישור כפתור רוחב אזור כפתור הגדרות כפתור גודל כפתור סגנון כפתור שדה כפתור תיבות סימון מסומן בחירת טופס: תוכן יש להעתיק ולהדביק את השורטקוד לתוך עורך הטקסט, או להשתמש בוידג'ט טפסים. יצירת טופס קרדיט				 סגנון מותאם אישית שדות מיוחדים נמחקו שדות מיוחדות עודכנו תאריך ברירת מחדל ברירת מחדל תפריט בחירה עריכת טופס אימייל אימייל תגובה (אופציונלי) כותרת אימייל אימייל אל כותרת שדה הגדרות שדה גודל שדה סגנון שדה סוג שדה שדות טופס אפשרויות טופס סגנון טופס מידע טופס טיוטת טופס עודכנה הטופס פורסם הטופס שוחזר לגרסה קודמת מ %s הטופס נשמר הטופס נשלח טופס עודכן טפסים אימייל השולח שם השולח לא להציג אם אתה משתמש בתוסף Akismet תוכל גם להשתמש בו במסנן ספאם לטופס על ידי הגדרות השדה. יש להזין את השורטקוד ID של השדה הרלוונטי לפי הההגדרות הבאות, לדוגמא: אימייל מחבר: [form-field-1] צבע טקסט שדה יש להזין שורטקוד ID טופס לא חוקי יישור כותרת צבע כותרת גודל כותרת גדול רשימה בשורה מקסימום בינוני מינימום שם לתגובה (אופציונלי) טופס חדש הודעה חדשה מאת "%s" לא נמצאו תוצאות לא נמצאו תוצאות בפח מספר אפשרות אחת בכל שורה אפשרויות קישור לעמוד טקסט בתוך השדה מופעל על ידי http://pojo.me/ תצוגה מקדימה תצוגה מקדימה יש בעיה עם הגדרות טופס אפשרויות בחירה הפניה אל עמוד תודה (אופציונלי) כתובת IP השולח שדה חובה שורות חיפוש טופס שליחה הגדר אפשרות ראשונה כבלתי ניתנת לבחירה הגדרות שורטקוד שורטקוד ID צדדי - יישור לימין צדדי - יישור לשמאל קטן מסנן ספאם טלפון טקסט צבע טקסט אזור טקסט תוסף הטפסים אינו נתמך בגרסה זו של ערכת העיצוב %s. יש <a href="%s">לשדרג את ערכת העיצוב לגרסה האחרונה</a>. שדה חובה בטופס זה ישנה שגיאה, אנא תקנו אותה. זמן כותרת טופס כותרת: מעל השדה קישור URL לא מסומן פרטי משתמש הצגת טופס רוחב גדול יותר ענק הפרטים נשלחו בהצלחה! בלוק מרכז ימין ללא שמאל 