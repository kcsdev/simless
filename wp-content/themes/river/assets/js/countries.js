$('select#input_3_1').on('change', function(){
$('div#talk_fields').html('');
 $('div#sms_fields').html('');
$('div#slide_fields').html('');
var country = $(this).val();  
 $.ajax({
            url:"/wp-admin/admin-ajax.php",
            type:'POST',
            data:'action=get_country&arg=' + country , 
            success:function(results)
            {
               var arr = jQuery.parseJSON(results);
              if(typeof arr.talk != "undefined"){
                var talk_output = '';
                talk_output += '<h4>דקות שיחה</h4>';
                talk_output += '<table border="1" style="width:50%">';
                $.each(arr.talk, function(index, value){
                  if(value.show_talk == true){
                    talk_output += '<tr>';
                    talk_output += '<td align="center">' + value.name_talk+'</td>';
                    talk_output += '<td align="center">' + value.talk_price+' $</td>';
                    talk_output += '</tr>';
                   }
                })
                 if(typeof arr.talk_comment != "undefined" && arr.talk_comment != '' && arr.talk_comment != null){
                 talk_output += '<tr>';
                    talk_output += '<td colspan="2"><h7>הערות</h7><br>' + arr.talk_comment+'</td>';  
                  talk_output += '</tr>';
                 }
                 talk_output += '</table>';
                 $('div#talk_fields').html(talk_output);
              }
              if(typeof arr.sms != "undefined"){
                var sms_output = '';
                sms_output += '<h4>sms</h4>';
                sms_output += '<table border="1" style="width:50%">';
                $.each(arr.sms, function(index, value){
                  if(value.show_sms == true){
                    sms_output += '<tr>';
                    sms_output += '<td align="center">' + value.sms_name+'</td>';
                    sms_output += '<td align="center">' + value.price_sms+' $</td>';
                    sms_output += '</tr>';
                   }
                
                
                })
             if(typeof arr.sms_comment != "undefined" && arr.sms_comment != '' && arr.sms_comment != null){
                 sms_output += '<tr>';
                    sms_output += '<td colspan="2"><h7>הערות</h7><br>' + arr.sms_comment+'</td>';  
                  sms_output += '</tr>';
                 }
                
                 sms_output += '</table>';
                 $('div#sms_fields').html(sms_output);
              }
              if(typeof arr.slide != "undefined"){
                var slide_output = '';
                slide_output += '<h4>גלישה</h4>';
                slide_output += '<table border="1" style="width:50%">';
                $.each(arr.slide, function(index, value){
                  if(value.slide_show == true){
                    slide_output += '<tr>';
                    slide_output += '<td align="center">' + value.slide_name+'</td>';
                    slide_output += '<td align="center">' + value.slide_price+' $</td>';
                    slide_output += '</tr>';
                   }
                
                
                })
            if(typeof arr.slide_comment != "undefined" && arr.slide_comment != '' && arr.slide_comment != null){
                 slide_output += '<tr>';
                    slide_output += '<td colspan="2"><h7>הערות</h7><br>' + arr.slide_comment+'</td>';  
                  slide_output += '</tr>';
                 }
                 slide_output += '</table>';
                 $('div#slide_fields').html(slide_output);
              }
            }
        });




});